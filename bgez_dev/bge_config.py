from typing import Optional, Mapping, Dict, Any
from pathlib import Path
import platform
import toml

from .utils import _get

__all__ = [
    'BGEConfig',
    '_find_bge_config',
]

BGEConfig = Dict[str, Any]

blender_python_bin = 'Scripts' if platform.system() == 'Windows' else 'bin'


def _find_bge_config(path: Path) -> BGEConfig:
    '''
    Looks for a valid bge configuration across the filesystem.
    '''

    stop = False
    while not stop:
        stop = path is path.parent

        pyproject_path = path / 'pyproject.toml'
        if pyproject_path.exists():
            with pyproject_path.open('r') as file:
                pyproject = toml.load(file)

            bge_config = _get_bge_config(pyproject, pyproject_path)
            if bge_config:
                break

        path = path.parent

    else:  # we did not break:
        raise FileNotFoundError('couldn\'t find a pyproject.toml file with a valid bge.runtime.blender entry')

    return bge_config


def _get_bge_config(pyproject: Mapping, pyproject_path: Path) -> Optional[BGEConfig]:
    '''
    Extracts a valid bge configuration from a pyproject.toml file.
    '''

    bge_config = _get(pyproject, 'bge', 'runtime')
    if bge_config is None:
        return None

    blender_property = _get(bge_config, 'blender')
    if blender_property is None:
        return None

    blender_path = Path(blender_property)
    if not blender_path.exists():
        print('%s : bge.runtime.blend\n  could not find: "%s"'
            % (str(pyproject_path), str(blender_path)))
        return None

    if not blender_path.is_dir():
        print('%s : bge.runtime.blend\n  not a directory: "%s"'
            % (str(pyproject_path), str(blender_path)))
        return None

    bge_config['blender'] = blender_path
    return _resolve_bge_config(bge_config)


def _resolve_bge_config(bge_config: BGEConfig) -> BGEConfig:
    blender_path = bge_config['blender']  # type: Path

    if 'python' not in bge_config:
        for path in (blender_path / 'python' / blender_python_bin).glob('python*'):
            bge_config['python'] = path
            break
        else:  # we did not break:
            raise FileExistsError('could not find a Python executable')

    return bge_config
